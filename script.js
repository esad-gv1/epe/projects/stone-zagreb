import { loadScripts } from "./js/utils.js";
import { paginate } from "./js/paginate.js";

let elementsToPaginate = [];

//CSS used in this project, including the pagedjs preview css
const styleList = [
    "css/style.css",
    "css/cover.css",
    "css/back.css",
    "vendors/css/paged-preview.css"
];

//additional script to load, not importable as es modules
const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js",
    "vendors/js/markdown-it-span.js",
];

//sync batch loading
await loadScripts(scritpList);

// console.log("markdownitContainer", markdownitContainer);

//markdown files to load
const mdFilesList = ["md/main.md"];
//html elements to be filled from converted md file
const partsList = ["content"];

//markdownit instanciation (old school method as no ES6 modules are available)
const markdownit = window.markdownit
    ({
        // Options for markdownit
        langPrefix: 'language-en',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer) //div
    .use(markdownitSpan) //span
    .use(markdownItAttrs, { //custom html element attributes
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });
// console.log(markdownit);

//function to produce the HTML from md files
async function layoutHTML() {

    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < mdFilesList.length; index++) {

        const mdFile = mdFilesList[index];
        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        const result = markdownit.render(mdContent);

        const destinationElement = document.getElementById(partsList[index]);
        destinationElement.innerHTML = result;
        //elementsToPaginate.push(destinationElement.cloneNode(true));
    };
}

//wait to have all the element loaded (module scripts can't be defered)
window.addEventListener("load", async (event) => {
    await layoutHTML();
    buildStonePage();
});

//interaction
const printBt = document.querySelector('#printBt');

printBt.addEventListener("click", () => {

    /* const bigContainers = [...document.getElementsByClassName("bigContainer")];
    elementsToPaginate = elementsToPaginate.concat(bigContainers); */
    const content = document.getElementById("content");
    elementsToPaginate = [...content.children];

    paginate(elementsToPaginate, styleList);
});

///////////////////////////////////////////

let pushDustSpan, pushDustDiv, partsHeight;
let dust = document.getElementsByClassName('dust');
let stones = Array.from(document.getElementsByClassName("stone"));
let parts = Array.from(document.getElementsByClassName("parts"));
const valuesTab = [];

function buildStonePage() {
    const fr = document.getElementsByClassName('fr');
    const cr = document.getElementsByClassName('cr');
    dust = document.getElementsByClassName('dust');
    const colors = ["#dcdcd8", "#f1d4cb", "#f5eee8", "#c9b8a7", "#9d9892", "#c0b3a9"]
    document.addEventListener('selectionchange', function () {
        var randomColor = colors[getRandomInt(0, 5)];
        document.documentElement.style.setProperty('--selection-color', randomColor);
    });


    parts = Array.from(document.getElementsByClassName("parts"));
    parts.forEach(part => {
        // partsHeight = part.offsetHeight;
        // console.log(part.offsetHeight + "hello");
        part.style.left = getRandomInt(0, (window.innerWidth - part.offsetWidth) / 2) + "px";
        console.log(window.innerWidth, part.offsetWidth);
    });
    stones = Array.from(document.getElementsByClassName("stone"));
    stones.forEach(stone => {
        stone.style.backgroundColor = colors[getRandomInt(0, 5)];
        stone.addEventListener("mouseover", spaned);
    });
    for (var i = 0; i < dust.length; i++) {
        partsHeight = dust[i].offsetHeight - 30;
        valuesTab[i] = [partsHeight, 60, 70];
    }
    document.addEventListener("mousedown", function (event) {
        if (event.target.classList.contains("fragment")) {
            chiseled(event);
            rotated(event);
        }
    });
    //////// hammer time
    const hammer = document.getElementById("hammer");
    document.addEventListener("mousemove", function (event) {
        hammer.style.left = (event.pageX - hammer.width / 2) + 10 + "px";
        hammer.style.top = (event.pageY - hammer.height / 2) + 10 + "px";
    });
    document.addEventListener("mousedown", function () {
        let rotation = -15;
        hammer.style.transform = "rotate(" + rotation + "deg)";
    });
    document.addEventListener("mouseup", function () {
        hammer.style.transform = "rotate(0deg)";
    });
}

/////// spaned the text on the fist hover
function spaned(event) {
    if (!event.target.getAttribute('hovered')) {
        var word = event.target.innerHTML.split(" ");
        var newSentence = [];
        word.forEach(w => {
            if (w.startsWith("<")) {
                newSentence.push(document.createElement("br"));
            } else {
                let newSpan = document.createElement("span");
                newSpan.textContent = w + " ";
                newSpan.classList.add("fragment");
                newSentence.push(newSpan);
            }
        })
        event.target.innerHTML = "";
        for (var i = 0; i < newSentence.length; i++) {
            if (newSentence[i] instanceof Element) {
                event.target.appendChild(newSentence[i]);
            } else {
                event.target.appendChild(document.createTextNode(newSentence[i]));
            }
        }
    }
    event.target.removeEventListener("mouseover", spaned);
}

/////// chiseled the spans on mouse down
function chiseled(event) {
    event.target.style.color = "transparent";
    event.target.style.backgroundColor = "white";
    // event.target.style.pointerEvents = "none";
    pushDustSpan = document.createElement("span");
    pushDustSpan.innerHTML = event.target.innerHTML;
    pushDustSpan.classList.add("dustSpan");
    pushDustDiv = event.target.parentNode.nextElementSibling;
    pushDustDiv.appendChild(pushDustSpan);
    pushDustSpan.style.backgroundColor = event.target.parentNode.style.backgroundColor;
    ////// position of the span
    const index = Array.from(dust).indexOf(event.target.parentNode.nextElementSibling);
    // console.log(event.target.parentNode);
    let leftValue = getRandomInt(valuesTab[index][1], valuesTab[index][2])
    const midpoint = (valuesTab[index][2] - valuesTab[index][1]) / 2;
    let distanceFromMidpoint = leftValue - valuesTab[index][1] - midpoint;
    if (distanceFromMidpoint < 0) {
        distanceFromMidpoint = -distanceFromMidpoint;
    }
    // console.log(valuesTab[index][0], dust[index].offsetHeight);
    pushDustSpan.style.top = valuesTab[index][0] + distanceFromMidpoint * 6 + "px";
    pushDustSpan.style.left = leftValue + "%";
    pushDustSpan.style.transform = "rotate(" + getRandomInt(0, 360) + "deg)";
    valuesTab[index][0] += -4;
    valuesTab[index][1] += -0.7;
    valuesTab[index][2] += 0.7;
}

function rotated(event) {
    var parent = event.target.parentNode;
    var parentPosition = parent.getBoundingClientRect();
    var x = -(parentPosition.left + parentPosition.width - event.clientX) + parentPosition.width;
    var y = -(parentPosition.top + parentPosition.height - event.clientY) + parentPosition.height;
    
    // Initialize angle and clearInterval for the parent if they don't exist
    parent.angle = parent.angle || 0;
    clearInterval(parent.intervalId);

    // Update the angle and log it
    parent.angle += 5;
    console.log(parent.angle);

    // Set transform origin and initial rotation
    parent.style.transformOrigin = x + "px " + y + "px";
    parent.style.transform = "rotate(" + parent.angle + "deg)";

    // Start the interval for continuous rotation for this parent
    parent.intervalId = setInterval(() => {
        if (parent.angle < 0) {
            parent.angle = -parent.angle - 1;
        } else if (parent.angle > 0) {
            parent.angle = -parent.angle + 1;
        } else if (parent.angle === 0) {
            clearInterval(parent.intervalId);
        }
        parent.style.transform = "rotate(" + parent.angle + "deg)";
    }, 300);
}

//////// random function
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}