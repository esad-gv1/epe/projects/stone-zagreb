
:::.box
:::

# STONE AND MASONRY IN CROATIA

## Introduction

<img id="hammer" type="image/svg+xml" src="assets/img/hammer.svg">

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
        La pierre, témoin silencieux du temps, est une source de richesse et un symbole de force dans toute la Croatie. Depuis l’Antiquité, la pierre est un élément clé qui a façonné la culture, l’identité et l’économie croates. À travers une analyse de l’histoire, de la diversité, des aspects économiques et de l’impact artistique et culturel de la pierre, nous explorons la richesse et la complexité de cette ressource précieuse.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
        Kamen, tihi svjedok vremena, izvor je bogatstva i simbol snage diljem Hrvatske. Od davnih vremena, kamen je bio ključni element koji je oblikovao hrvatsku kulturu, identitet i gospodarstvo. Kroz analizu povijesti, raznolikosti, ekonomskih aspekata te umjetničkog i kulturnog utjecaja kamena, istražujemo bogatstvo i složenost ovog dragocjenog resursa.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">Depuis la préhistoire, où la pierre naturelle était utilisée pour fabriquer des outils et des armes, puis comme matériau de construction jusqu'à aujourd'hui, l'extraction et l'utilisation de la pierre sont considérées comme l'une des activités humaines les plus anciennes. En témoignent de nombreux objets et monuments historiques partout dans le monde. Des preuves matérielles de l'utilisation de la pierre ont également été trouvées dans la région de la Croatie actuelle. La maçonnerie s'est développée en Croatie à l'époque gréco-illyrienne et romaine. Les plus anciens bâtiments en pierre connus sont les tumulus illyriens et les murs des colonies grecques des îles de Vis et de Hvar. Les premières carrières de Brač datant de l'époque romaine sont Plata, Rasoha et Stražišća près de Škripa et Splitska. Ces carrières ont servi à construire Salona et le palais de Dioclétien.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">Od predhistorijskih vremena kada je prirodni kamen korišten za izradu oruđa i oružja, kasnije kao materijal za gradnju pa sve do danas, vađenje i upotreba kamena smatra se jednom od najstarijih ljudskih djelatnosti. O tome svjedoče brojni povijesni artefakti i spomenici posvuda u svijetu. Na prostorima su današnje Hrvatske također pronađeni materijalni dokazi o korištenju kamena. Klesarstvo se razvijalo u Hrvatskoj u Grčko-Ilirskom i Rimskom dobu. Najstarije poznate kamene građevine su ilirske gomile, pa zidine grčkih kolonija na otocima Visu i Hvaru. Prvi kamenolomi na Braču iz Rimskog doba su Plate, Rasohe i Stražišća u blizini Škripa i Splitske. Ti su kamenolomi korišteni za izgradnju Salone i Dioklecijanove palače.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Tout au long de l'histoire, des variétés sédimentaires de haute qualité, de composition principalement calcaire, ont été exploitées et utilisées comme pierre de construction naturelle ou architecturale dans la Croatie d'aujourd'hui. Aujourd'hui, 44 variétés différentes de pierre naturelle sont exploitées sous forme de blocs ou de monolithes, qui sont classés comme « marbre » sur le marché. Les blocs sont ensuite traités et transformés afin de pouvoir être utilisés dans la construction, l'architecture ou pour la production de nombreux objets utiles et décoratifs.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Tijekom povijesti na prostorima današnje Hrvatske, visokokvalitetni sedimentni varijeteti kamena, po sastavu pretežno vapnenci, eksploatirani su i korišteni kao prirodni ili arhitektonsko-građevni kamen. U današnje se vrijeme eksploatira 44 različita varijeteta prirodnoga kamena u obliku blokova odnosno monolita koji se na tržištu svrstavaju u skupinu “mramora”. Blokovi se zatim prerađuju i dodatno obrađuju da bi se mogli koristiti u graditeljstvu, arhitekturi ili za izradu mnogih uporabnih i ukrasnih predmeta.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Beaucoup de nos variétés de pierre naturelle : Kirmenjak avorio, Kanfanar, Adria grigio ou sivac, Veselje avec les variétés unito et fiorito, Benkovački pločasti et Vrnik, ont été utilisées pour la construction de nombreux monuments culturels et vieilles villes telles que Dubrovnik, Split, Solin, Zadar, Trogir, Šibenik, Pula, Poreč, Rab, Korčula et Hvar. Il convient de souligner que certaines de ces variétés étaient également utilisées dans des villes hors de Croatie, comme Venise, Vienne, Budapest et d'autres.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Mnogi su naši varijeteti prirodnoga kamena: Kirmenjak avorio, Kanfanar, Adria grigio ili sivac, Veselje s varijetetima unito i fiorito, Benkovački pločasti i Vrnik, korišteni za izgradnju brojnih spomenika kulture te starih gradova poput Dubrovnika, Splita, Solina, Zadra, Trogira, Šibenika, Pule, Poreča, Raba, Korčule i Hvara. Treba naglasiti da su neki od tih varijeteta korišteni i u gradovima izvan granica Hrvatske kao što su Venecija, Beč, Budimpešta i drugi.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## History

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">Les méthodes d'extraction de la pierre des gisements naturels ont changé au cours de l'histoire suite au développement d'outils pour creuser, couper, percer et enlever la pierre. Dès l'époque des Illyriens, on trouve des bâtiments avec de grandes dalles de pierre qui correspondent à l'épaisseur des couches calcaires de notre région. Ces blocs étaient séparés par des leviers le long de discontinuités naturelles, ce qui était facilement applicable dans les dépôts calcaires à couches horizontales ou légèrement inclinées.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Metode vađenja kamena iz prirodnih ležišta mijenjale su se kroz povijest prateći razvitak alatki za kopanje, rezanje, bušenje i odvaljivanje kamena. Iz doba Ilira na našim područjima nalazimo građevine s velikim pločastim blokovima kamena koji odgovaraju debljinama slojeva vapnenaca. Takvi su se blokovi odvajali polugama duž prirodnih diskontinuiteta, što je bilo lako primjenjivo u naslagama vapnenaca s horizontalno ili blago nagnutim slojevima.  
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Pour couper la pierre, on utilisait d'abord différents outils en pierre plus dure, puis en métal avec l'ajout de poudre abrasive. Le fractionnement de la pierre est un processus par lequel les gros blocs sont séparés de la masse rocheuse, qui sont ensuite traités. Pour fendre, des cales de bois sèches enfoncées dans des fissures ou des trous naturels ont été utilisées, qui ont ensuite été versées avec de l'eau. Le bois trempé a gonflé, augmentant la pression qui a provoqué la fissure de la pierre.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Za rezanje kamena služile su najprije različite alatke od tvrđeg kamena, a potom od metala uz dodavanje abrazivnog praha. Cijepanje kamena je postupak kojim se od stijenske mase odvajaju veliki blokovi koji se naknadno obrađuju. Za cijepanje su se rabili suhi drveni klinovi utisnuti u prirodne pukotine ili izdubljene rupe, koji su se nakon toga polijevali vodom. Natopljeno drvo je bubrilo, čime se povećavao tlak koji je uzrokovao cijepanje kamena.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    En raison de l'augmentation de la demande dans les carrières, la méthode d'exploitation des blocs de pierre par dynamitage contrôlé avec de la poudre noire a été introduite à titre expérimental, mais elle a été rapidement abandonnée en raison des dommages causés à la masse rocheuse utile par le dynamitage. . La scie à fil hélicoïdal a commencé à être utilisée au milieu du XXe siècle et, grâce à son utilisation, la production artisanale de pierre s'est transformée en production industrielle.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Povećanjem potražnje u kamenolome je probno uvedena metoda eksploatacije kamenih blokova kontroliranim miniranjem crnim barutom, ali je ubrzo napuštena zbog oštećenja korisne stijenske mase uslijed otpucavanja. Helikoidalna žična pila počinje se koristiti sredinom 20 st. Njezinom uporabom zanatska proizvodnja kamena prelazi u industrijsku.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La scie à fil hélicoïdal fonctionne selon le principe de l'abrasion avec du sable de quartz présent dans les rainures et dont les grains coupent la roche mère plus tendre. Avec l’apparition des tronçonneuses à chaîne et des scies à fil diamanté dans la seconde moitié du XXe siècle, une nouvelle ère technologique s’ouvre dans les carrières. Les nouvelles machines travaillaient beaucoup plus rapidement avec une consommation d'eau nettement inférieure.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Helikoidalna žična pila radi na principu abrazije kvarcnim pijeskom koji se nalazi u utorima, čija zrnca režu mekšu matičnu stijenu. Pojavom lančanih sjekačica i dijamantnih žičnih pila u drugoj polovini 20. st. u kamenolomima započinje nova tehnološka era. Novi strojevi su radili puno brže uz značajno manji utrošak vode.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Deposits of stone

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La pierre en tant que matière première minérale est classée selon son application en deux groupes : la pierre naturelle ou architecturale de construction et la pierre technique de construction, c'est-à-dire les granulats de pierre. En République de Croatie, ces deux groupes représentent les matières premières minérales non métalliques les plus importantes exploitées. La pierre naturelle est exploitée sous forme de blocs de pierre réguliers et « sains », c'est-à-dire des monolithes, et est utilisée pour la sculpture, la maçonnerie, l'architecture de cimetière, ainsi que pour le revêtement extérieur et intérieur horizontal et vertical de divers bâtiments de construction.
    </div>
    <div class="fr dust">
</div>
</div><div class="cr bigContainer">
    <div class="cr stone">
    Kamen kao mineralna sirovina se klasificira prema primjeni na dvije grupe i to: prirodni ili arhitektonsko-građevni i tehničko-građevni kamen odnosno kameni agregat. U Republici Hrvatskoj te dvije grupe predstavljaju najvažnije nemetalne mineralne sirovine koje se eksploatiraju. Prirodni kamen se eksploatira u obliku pravilnih i “zdravih” kamenih blokova odnosno monolita te se koristi za kiparstvo, klesarstvo, arhitekturu groblja kao i za horizontalna i vertikalna vanjska i unutarnja oblaganja raznih građevinskih objekata.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Selon la liste des champs d'exploitation actifs en République de Croatie de 2019, des variétés de pierre naturelle sont exploitées dans 91 gisements actifs. Parmi le nombre de gisements mentionnés, des variétés sous forme de blocs de pierre sont extraites de 69 d'entre eux, tandis que des variétés sous forme de plaques minces pouvant atteindre plusieurs centimètres d'épaisseur sont exploitées dans 22 champs.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Prema popisu aktivnih eksploatacijskih polja u Republici Hrvatskoj iz 2019. varijeteti prirodnoga kamena se eksploatiraju na 91 aktivnom ležištu. Od spomenutog broja ležišta iz njih 69 se vade varijeteti u obliku kamenih blokova, dok se iz 22 polja eksploatiraju varijeteti u obliku tankih ploča koje mogu biti debele nekoliko centimetara.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Selon les données de la norme croate et européenne HRN EN 12440, 44 variétés différentes de pierre naturelle « domestique » peuvent être reconnues aujourd'hui sur le marché. Nos variétés « domestiques » sont classées sur le marché dans le groupe « marbre », bien que selon leur origine elles appartiennent à des roches sédimentaires. Les roches d'origine magmatique et métamorphique (amphibolites, andésites, basaltes, diabases, granites) ne sont exploitées en Croatie que comme pierre technique de construction. Parmi nos variétés de « marbre » que l’on retrouve sur le marché, le calcaire est la plus représentée – soit un total de 31 variétés. Les calcaires dolomitisés (sept variétés), les conglomérats calcaires (trois variétés), les brèches calcaires (deux variétés) et les brèches dolomitiques (une seule variété) sont représentés en plus petit nombre.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Prema podacima iz hrvatske i europske norme HRN EN 12440, na tržištu se danas može prepoznati 44 različita “domaća” varijeteta prirodnoga kamena. Naši “domaći” varijeteti se na tržištu svrstavaju u skupinu “mramora” iako prema postanku pripadaju sedimentnim stijenama. Stijene magmatskog i metamorfnog postanka (amfiboliti, andeziti, bazalti, dijabazi, graniti) se u Hrvatskoj eksploatiraju samo kao tehničko-građevni kamen. Među našim varijetetima “mramora” koji se mogu pronaći na tržištu najzastupljeniji su vapnenci – ukupno 31 varijetet. U manjem broju zastupljeni su dolomitizirani vapnenci (sedam varijeteta), vapnenački konglomerati (tri varijeteta), vapnenačke breče (dva varijeteta) i dolomitna breča (samo jedan varijetet).
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Les gisements de nos variétés se trouvent principalement dans la partie côtière de l'Istrie, de la Dalmatie et des îles de Brač et Korčula. Dans la région de l'Istrie et de l'île de Brač, 26 variétés de pierre sont exploitées, qui seront décrites dans les chapitres suivants. 15 variétés sont exploitées en Dalmatie (Alkasin, Benkovački pločasti, Dolit, Fantazija, Jadran zeleni, Marići, Mironja, Multikolor, Negris fiorito, Plano, Romanovac, Rozalit, Seget, Visočani, Vrsine). Les calcaires de San Antonio et de Vrnik sont exploités sur l'île de Korčula, tandis qu'une seule variété – le calcaire poreux Vinicite – est exploitée dans la partie continentale de la Croatie, près de Varaždin. Il convient de souligner que la liste des variétés de pierre naturelle dans la norme mentionnée ci-dessus n'est pas exhaustive, car hormis la dalle Benkovac, d'autres variétés de dalles ne sont pas répertoriées, comme le calcaire Lipovec, qui est extrait sous forme de dalles minces de gisements près de la ville de Samobor.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Ležišta naših varijeteta nalaze se uglavnom u primorskom dijelu na području Istre, Dalmacije, te otoka Brača i Korčule. Na području Istre i otoka Brača  ekploatira se 26 varijeteta kamena, koja će biti opisana u sljedećim poglavljima. Na području Dalmaciji se eksploatira 15 varijeteta (Alkasin, Benkovački pločasti, Dolit, Fantazija, Jadran zeleni, Marići, Mironja, Multikolor, Negris fiorito, Plano, Romanovac, Rozalit, Seget, Visočani, Vrsine). Na otoku Korčuli se eksploatiraju vapnenci San Antonio i Vrnik, dok se samo jedan varijetet – porozni vapnenac Vinicit eksploatira u kontinentalnom dijelu Hrvatske pored Varaždina. Treba naglasiti da popis varijeteta prirodnoga kamena u gore navedenoj normi nije konačan, jer se osim Benkovačkog pločastog ne navode drugi pločasti varijeteti poput Lipovečkog vapnenca, koji se vadi u obliku tankih ploča iz ležišta u blizini grada Samobora.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Stoneworking - Istria

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    L'Istrie est une région connue pour ses nombreuses carrières où étaient extraites autrefois et encore aujourd'hui des variétés de pierre naturelle d'une qualité exceptionnelle. En raison de leur utilisation à long terme, les variétés istriennes sont connues non seulement dans notre pays, mais aussi dans le monde car elles ont été utilisées dans de nombreux monuments célèbres.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Istra je regija koja je poznata po svojim brojnim kamenolomima u kojima su se tijekom prošlosti vadili, a i danas se vade iznimno kvalitetni varijeteti prirodnoga kamena. Istarski varijeteti su zbog svoje dugotrajne upotrebe poznati ne samo kod nas, nego i u svijetu jer su upotrijebljeni u mnogim znamenitim spomenicima.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Les variétés commerciales exploitées sont : Grožnjan, Istranka, jaune d'Istrie avec les versions Kanfanar, Selina et Korenići, Kirmenjak, Lucija, Marčana, Planik et Valtura avec les versions unito et fiorito et Vinkuran avec les versions fiorito et statuario. Toutes les variétés répertoriées sont déterminées comme des calcaires d'âges différents depuis le Jurassique (Kirmenjak), en passant par le Crétacé (Grožnjan, Istarski žuti, Lucija, Marčana, Valtura et Vinkuran) jusqu'à l'Éocène (Istranka, Planik).
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Komercijalni varijeteti koji se eksploatiraju su: Grožnjan, Istranka, Istarski žuti s inačicama Kanfanar, Selina i Korenići, Kirmenjak, Lucija, Marčana, Planik te Valtura s inačicama unito i fiorito i Vinkuran s inačicama fiorito i statuario. Svi navedeni varijeteti su determinirani kao vapnenci različite starosti od jurske (Kirmenjak), preko kredne (Grožnjan, Istarski žuti, Lucija, Marčana, Valtura i Vinkuran) do eocenske (Istranka, Planik).
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La variété la plus célèbre d'Istrie est le Kirmenjak ou Pietra d'Istria, qui est définie comme un calcaire micritique stylolitisé de couleur ivoire du Jurassique supérieur. En raison de sa couleur, on l'appelle souvent ivorio ou avorio. La pierre fait preuve d'une stabilité exceptionnelle même dans des conditions particulièrement défavorables, comme lorsqu'elle est exposée au sel marin. Cette pierre dense présente une solidité et une résistance à l’usure exceptionnelles, ainsi qu’une faible porosité et une faible absorption d’eau. Le Kermenjak a été utilisé comme pierre de construction dans de nombreuses constructions à Venise (Ponte di Rialto, Palais des Doges), généralement dans des zones sensibles où il est constamment exposé aux marées et alterne mouillage et séchage. Il a conservé jusqu’à ce jour son bel aspect, ce qui prouve sa durabilité exceptionnelle.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Najpoznatiji varijetet iz Istre je Kirmenjak ili Pietra d’Istria koji je određen kao stilolitizirani mikritni vapnenac gornjojurske starosti boje slonovače. Upravo zbog svoje boje često se naziva i ivorio ili avorio. Kamen pokazuje iznimnu postojanost čak i u posebno nepovoljnim uvjetima kao na primjer prilikom djelovanja morske soli. Ovaj gusti kamen je iznimne čvrstoće i otpornosti na habanje, te male poroznosti i malog upijanja vode. Kirmenjak se koristio kao građevni kamen u brojnim konstrukcijama u Veneciji (Ponte di Rialto, Duždeva palača), obično u osjetljivim dijelovima gdje je stalno izložen plimi i oseki te naizmjeničnom vlaženju i sušenju. Do danas je sačuvao dobar izgled što dokazuje njegovu iznimnu postojanost.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La deuxième variété la plus célèbre d'Istrie est le jaune d'Istrie/Giallo d'Istria/Kanfanar, qui est exploitée dans les carrières de Kanfanar, Selina et Korenići, et différentes versions de cette pierre portent le nom des carrières. Il est important de noter que ces carrières sont les seules mines souterraines de la République de Croatie. On sait que ce type de pierre provenait de l'île de Sv. Jerolim (Îles Brijun) transporté à Ancône sur recommandation de Juraj Dalmatinac. Kanfanar est un calcaire de couleur jaune dont il tire son nom, car il se reconnaît à l'alternance notable de parties plus claires et plus foncées. La partie la plus claire de la roche est constituée de micrite, tandis que la partie la plus sombre est constituée de grains oncoïdes irrégulièrement enveloppés créés par l'action du « microproblème » Bacinella irregularis RADOIČIĆ, c'est pourquoi on les appelle bacinella oncoïdes. Le jaune d’Istrie est classé comme un calcaire de type floutstone oncoïde.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Drugi najpoznatiji varijetet iz Istre je Istarski žuti/Giallo d’Istria/Kanfanar koji se eksploatira u kamenolomima Kanfanar, Selina i Korenići, te se prema kamenolomima i nazivaju različite inačice tog kamena. Bitno je napomenuti da se u tim kamenolomima obavlja jedina podzemna eksploatacija u Republici Hrvatskoj. Poznato je da se ovaj tip kamena u 15. stoljeću s otočića Sv. Jerolim (Brijunski otoci) prevozio u Ankonu po preporuci Jurja Dalmatinca. Kanfanar je vapnenac žute boje po kojoj je dobio i ime, s time da je prepoznat po uočljivoj izmjeni svjetlijih i tamnijih dijelova. Svjetliji dio stijene čini mikrit, dok tamniji dio čine nepravilno obavijena zrna onkoida nastala djelovanjem “mikroproblematike” Bacinella irregularis RADOIČIĆ pa se nazivaju baćinelski onkoidi. Istarski žuti je klasificiran kao vapnenac tipa onkoidni floutston.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Stone quarrying in Istria

 <!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La pierre est exploitée en Istrie depuis des siècles pour deux objectifs fondamentaux. Le premier est la production de pierre de construction technique pour la construction de routes et la réhabilitation de berges, et le deuxième est l'exploitation de pierre de construction architecturale adaptée à la production d'éléments pour la construction de maisons, d'édifices religieux et autres. Les bâtiments en pierre les plus anciens connus sont les kažuni d'Istrie, construits avec de la pierre naturelle non traitée sans utilisation de liant, ce qu'on appelle technique de cloison sèche. Kažuni sont des témoins du passé où les habitants étaient engagés dans l'agriculture, notamment l'oléiculture et la viticulture.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Kamen je u Istri stoljećima eksploatiran radi dvije osnovne svrhe. Prva je proizvodnja tehničko-građevnog kamena za izgradnju cesta i saniranje obala, a druga eksploatacija arhitektonsko-građevnog kamena pogodnog za izradu elemenata za gradnju kuća, sakralnih i drugih građevina. Najstarije poznate kamene građevine su istarski kažuni zidani neobrađenim prirodnim kamenom bez uporabe vezivnog materijala tzv. tehnikom suhozida. Kažuni su svjedoci prošlih vremena kada su se stanovnici bavili poljoprivredom, posebice maslinarstvom i vinogradarstvom.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La période antique est connue pour l'utilisation de la pierre pour la construction de bâtiments romains monumentaux, d'aqueducs, de ponts et de sculptures. Les cafés anciens les plus célèbres sont le "Cava Romana", c'est-à-dire les carrières de Vinkuran et Vintijan en Istrie, au sud de Pula, où étaient extraits les calcaires du Crétacé. L'objet le plus célèbre de cette époque est l'amphithéâtre, communément appelé l'Arène de Pula, qui a été construit au 1er siècle à l'époque de l'empereur romain Vespzian. La pierre a été livrée depuis les îles Brijuni, c'est-à-dire l'île de Saint-Pierre. Jerolim dans la baie de Fažana et de la baie Saline, au sud de Rovinj. Parmi les monuments les plus importants se distingue l'arc de triomphe de Sergueï à Pula. Des calcaires jurassiques qui se présentent sous forme de dalles et de dalles plus minces ont été exploités près de Poreč et ont été utilisés pour construire le mausolée de Théodore à Ravenne. Les premières données sur le transport de la pierre datent de 812 et disent que la pierre en blocs était transportée par bateau depuis l'Istrie jusqu'aux ports romains d'Aquilée et de Ravenne pour la construction du tombeau du souverain Théodoric.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Antičko je razdoblje poznato po upotrebi kamena za građenje monumentalnih rimskih građevina, akvadukata, mostova i skulptura. Najpoznatije antičke kave su “Cava Romana”, odnosno kamenolomi Vinkuran i Vintijan u Istri, južno od Pule, gdje su se vadili kredni vapnenci. Najpoznatiji objekt tog doba je amfiteatar, popularno nazvan Arena u Puli, koja je građena u 1. st. za vrijeme rimskog cara Vespzijana. Kamen je dopreman iz Brijunskih otoka, odnosno otoka sv. Jerolima u Fažanskom zaljevu i iz zaljeva Saline, južno od Rovinja. Od značajnijih spomenika još se ističe i Sergejev slavoluk u Puli. Jurski vapnenci koji se pojavljuju u obliku ploča i tanjih ploča eksploatirali su se u blizini Poreča i njime je građen Teodorov mauzolej u Raveni. Prvi podaci o prijevozu kamena datiraju iz 812. godine i govore kako se kamen u blokovima prevozio brodovima iz Istre u rimske luke Akvileju i Ravenu za izgradnju grobnice vladara Teodorika.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Les premières données enregistrées sur l'exploitation, encore active aujourd'hui, de la carrière de Kirmenjak, alors appelée Orsera (Montraker), remontent à l'an 568. C'est alors qu'a commencé la construction de Venise, qui doit sa beauté principalement à la pierre, à ses caractéristiques, sa qualité et sa couleur. Selon certains documents, on estime que jusqu'à 80 % de la ville de Venise a été construite avec de la pierre provenant des carrières situées près de Vrsar et de Zlatni rata, près de Rovinj. En raison de ses extraordinaires caractéristiques physiques et mécaniques et de sa résistance à l’action de l’eau de mer, cette pierre est devenue le seul matériau autorisé à être installé sur les façades des églises, des palais, des édifices civils, religieux et militaires. Le Palais des Doges de Venise est en grande partie construit en pierre d'Istrie. La pierre de Kirmenjak, ainsi que la pierre de la région de Buje, ont été utilisées dans la construction de la basilique euphrasienne paléochrétienne de Poreč, qui remonte au VIe siècle. siècle. À cette époque, le commerce de la pierre d'Istrie était déjà réglementé par des lois sur la base desquelles quiconque remplaçait cette pierre par une pierre similaire était puni. Au fil des années, la valeur de la pierre d’Istrie a augmenté. Les Romains envoyèrent leurs tailleurs de pierre à Vrsar pour exploiter la pierre qui servit plus tard à construire Venise. Au fil des siècles, l’intensité de l’extraction de pierre s’est accrue à mesure que les besoins de l’Empire romain augmentaient. La première division enregistrée de la pierre d'Istrie remonte à 1615 en : Bianca fine, Bianca cinerina, Bianca fumicata.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Prvi zabilježeni podaci eksploatacije, i danas aktivnog, kamenoloma Kirmenjak, tada zvanog Orsera (Montraker), datiraju još od 568. godine. Tada je započela gradnja Venecije, koja svoju ljepotu najviše duguje kamenu, njegovim karakteristikama, kvaliteti i boji. Prema nekim zapisima vjeruje se da je čak 80% grada Venecije izgrađeno kamenom iz kamenoloma kod Vrsara i Zlatnog rata kod Rovinja. Zbog svojih izvanrednih fizičko-mehaničkih karakteristika te otpornosti na djelovanje morske vode ovaj je kamen postao jedini materijal koji je bilo dopušteno ugrađivati u pročelja crkvi, palača, civilnih, vjerskih i vojnih građevina. Duždeva palača u Veneciji velikim dijelom izgrađena je od istarskog kamena. Kamen Kirmenjak, kao i kamen iz okolice Buja korišten je pri gradnji starokršćanske Eufrazijeve bazilike u Poreču koja datira iz 6. stoljeća. Trgovina istarskim kamenom već je u to doba bila regulirana Statutima, na temelju kojih je bio kažnjen svatko tko taj kamen zamjeni sličnim kamenom. Tijekom godina vrijednost istarskog kamena je rasla. Rimljani su slali svoje klesare u Vrsar kako bi eksploatirali kamen koji je kasnije služio za izgradnju Venecije. Tijekom stoljeća intenzitet vađenja kamena raste kako su se povećavale potrebe Rimskog carstva. Prva zabilježena podjela istarskog kamena veže se za 1615. godinu na: Bianca fine, Bianca cinerina, Bianca fumicata.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Avec le départ des Romains, l'utilisation de la pierre naturelle a été considérablement réduite dans toutes les régions de la Croatie. Dans les périodes préromane et romane, avec l'acceptation du christianisme, la pierre était utilisée pour la construction d'édifices sacrés et de mobilier d'église, et il s'agissait principalement de pierres autochtones provenant de bâtiments anciens. À l'époque gothique, de petites églises étaient construites dans les villages d'Istrie, dans lesquelles le style roman était combiné avec des éléments décoratifs gothiques.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Odlaskom Rimljana upotreba prirodnog kamena znatno je smanjena u svim dijelovima Hrvatske. U predromanskom i romanskom razdoblju, prihvaćanjem kršćanstva, kamen je korišten za gradnju sakralnih građevina i crkvenog namještaja, a uglavnom se radilo o autohtonom kamenu uzimanom iz antičkih građevina. U doba gotike po istarskim selima su se gradile male crkvice u kojima se romanika povezivana s gotičkim dekorativnim elementima.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    L'exploitation souterraine de la pierre, selon les archives, a eu lieu vers 1600 à l'intérieur de l'Istrie, dans la région de Buzeština. On pense que ces informations font référence à la carrière souterraine actuelle de Sveti Stjepan, et la pierre de cette carrière est appelée « granit d'Istrie » en raison de sa texture.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Podzemna eksploatacija kamena, prema zapisima, odvijala se oko 1600. godine u unutrašnjosti Istre na području Buzeštine. Vjeruje se kako se ovaj podatak odnosi na današnji podzemni kamenolom Sveti Stjepan, a kamen iz tog kamenoloma zbog svoje teksture naziva se “granit iz Istre”.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Au début du XIXe siècle et sous la domination de la monarchie austro-hongroise, les carrières de Marčana, Planik et d'autres furent ouvertes, et la pierre extraite dans ces localités fut souvent utilisée dans la construction de divers bâtiments de la monarchie. Dans la seconde moitié du XIXe siècle, dans les environs de Žminj, on exploitait des pierres adaptées à la construction de bâtiments, de détails architecturaux et de monuments. Dix carrières furent ouvertes et la pierre fut exportée vers l’Amérique, l’Italie et même la Perse et d’autres pays lointains. Jusqu'à la fin de 1945, les carrières suivantes étaient exploitées en Istrie : Valkarin, Žminj, Turska vala, Montraker, Sv. Stjepan, Bale, Kirmenjak, Kloštar, Valtura, Vinkuran, Funtana, Negrin, Grožnjan et bien d'autres. Après la Première Guerre mondiale, sur un total de 20 entreprises enregistrées pour l'exploitation de la pierre, seules trois restèrent actives, qui furent regroupées en 1954 dans la société Kamen d.d. Pazin qui fonctionne encore aujourd'hui.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Početkom 19. stoljeća i dominacijom Austro-Ugarske monarhije dolazi do otvaranja kamenoloma Marčane, Planika i drugih, a kamen vađen na tim lokalitetima često je korišten pri gradnji raznih objekata u Monarhiji. U drugoj polovici 19. stoljeća u okolici Žminja vadio se kamen pogodan za gradnju zgrada, arhitektonskih detalja i spomenika. Otvoreno je deset kamenoloma, a kamen se izvozio u Ameriku, Italiju, pa čak i Perziju i druge daleke zemlje. U razdoblju do kraja 1945. godine u Istri djeluju kamenolomi: Valkarin, Žminj, Turska vala, Montraker, Sv. Stjepan, Bale, Kirmenjak, Kloštar, Valtura, Vinkuran, Funtana, Negrin, Grožnjan i mnogi drugi. Nakon 1. svjetskog rata od ukupno 20 tvrtki registriranih za eksploataciju kamena ostale su djelovati su samo tri koje su ujedinjene 1954. godine u tvrtku Kamen d.d. Pazin koja djeluje i danas.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Masonry in Istria

 <!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La maçonnerie est un métier qui, à toutes les époques historiques et stylistiques, avait le même objectif : donner vie à la pierre avec habileté. A partir des tailleurs de pierre et maçons autodidactes, les conditions d'un apprentissage organisé et polyvalent de ce métier furent progressivement acquises. Dans diverses régions et cultures, en fonction de la dureté, de la résistance et de la couleur, la pierre était traitée de différentes manières, ce qui est devenu une tradition selon la méthode de traitement. De nombreux documents témoignent du développement de l'artisanat de la taille de pierre sous l'Empire romain et en Croatie même à l'époque gréco-illyrienne. En témoignent les gros blocs de pierre intégrés aux murs défensifs. La culture de Hallstatt a également été trouvée à Vivača en Istrie, dès le 6ème siècle avant JC. n. c'est-à-dire avec des sculptures en pierre, probablement du sanctuaire et une partie conservée de la sculpture équestre. À l’époque de l’Empire romain, lorsque la pierre connaissait son apogée, les œuvres en pierre étaient rares. Les pierres travaillées que l'on retrouvait alors sur les bâtiments provenaient probablement d'édifices anciens. Il est très difficile de séparer la sculpture de l’utilisation de la pierre dans la construction, sachant que la pierre d’Istrie était à l’origine utilisée pour recouvrir les surfaces des bâtiments de décorations rares. A proximité de la basilique romane St. Marije, sur l'îlot devant Vrsar, on peut voir des traces de carrières abandonnées (au sommet de l'îlot Saint-Georges). Certains historiens de l'art postulent que le dôme monumental et monolithique du mausolée de l'Antiquité tardive du roi gothique oriental Théodoric le Grand (493-526) à Ravenne était constitué de calcaire d'Istrie provenant de la carrière de Saint-Pierre. Nikola près de Poreč, où la pierre était extraite dans l'Antiquité et l'Antiquité tardive. Au moment de sa pleine activité à Venise, le sculpteur de la Renaissance Antonio Rizzo (1467 – 1498) de Vérone est venu à Rovinj et Vrsar, qui a laissé à Venise ses œuvres les plus importantes, comme les travaux sur le Palais des Doges et la pierre tombale du Doge. Tron. Au XVe siècle, les tailleurs de pierre Zuanne et Bartolomeo Bon ont sculpté la statue "Saint Marc en forme de Léon" dans le café de Rovinj pour 1 700 ducats.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Klesarstvo je zanimanje koje je u svim povijesnim i stilskim razdobljima imalo isti cilj: vještinom kamenu udahnuti život. Počevši od samoukih kamenara i klesara stekli su se postupno uvjeti za organizirano i svestrano učenje ovog zanata. U raznim krajevima i kulturama, ovisno o tvrdoći, čvrstoći i boji, kamen se obrađivao različitim načinima koji su prerasli u tradicije po načinu obrade. Mnogi dokumenti svjedoče razvijenom kamenoklesarskom obrtu u doba Rimskog Carstva, a u Hrvatskoj još u grčko-ilirsko doba. O tome svjedoče veliki klesanci ugrađivani u obrambene zidove. Iz tog je vremena i nalaz halštatske kulture u Vivači u Istri, iz 6. st. pr. n. e. s kamenim skulpturama, vjerojatno iz svetišta i očuvanim dijelom skulpture konjanika. U doba Rimskog Carstva, kada kamenarstvo doživljava svoj procvat, klesarska su djela rijetka. Obrađeni kamen koji se tada nalazio na građevinama je vjerojatno potjecao od antičkih građevina. Vrlo je teško razdvojiti kiparstvo i upotrebu kamena u građevinarstvu s obzirom da se istarski kamen prvobitno upotrebljavao za oblaganja površina na građevinama uz rijetke dekoracije. U blizini romaničke bazilike Sv. Marije, na otočiću pred Vrsarom, vide se tragovi napuštenih kamenoloma (na vrhu otočića Sv. Jurja). Neki povjesničari umjetnosti iznose pretpostavku da je monumentalna i monolitna kupola kasnoantičkog mauzoleja istočno-gotskog kralja Teodorika Velikog (493. – 526.) u Raveni napravljena od istarskog vapnenca iz kamenoloma sv. Nikola kod Poreča, u kojem se vadio kamen u doba antike i kasne antike. U doba svoje pune aktivnosti u Veneciji, u Rovinj i Vrsar dolazio je renesansni kipar Antonio Rizzo (1467. – 1498. g.) iz Verone, koji je Veneciji ostavio svoja najvažnija djela kao što su radovi na Duždevoj palači i nadgrobni spomenik duždu Tronu. U 15. stoljeću klesari Zuanne i Bartolomeo Bon su za 1700 dukata u rovinjskoj kavi isklesali kip “San Marco in forma di Leon”.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Les débuts de la taille de pierre, telle que nous la connaissons aujourd'hui en Istrie, remontent à 1723, lorsque pour la première fois les tailleurs de pierre furent séparés des mineurs. L'art de décorer les ciboires d'églises, les cloisons d'autel et les chaires avec les motifs prédominants de l'osier est en train d'émerger. La première coopérative de tailleurs de pierre d'Istrie a été fondée en 1905 à Tarska Vala près de Novigrad. Les tailleurs de pierre qui ont étudié leur métier dans le berceau de la taille de pierre, en Italie, y ont travaillé. Différentes orientations commencent à émerger dans toutes les régions de l’Istrie, et cette tendance persiste jusqu’à ce jour. Vrsar porte fièrement le nom de « ville de Chypre ». L'école de sculpture de Montraker, qui tire son nom de l'ancienne carrière du même nom, est particulièrement importante. Selon certaines données, la pierre de Rovinj-Vrsar était également utilisée par d'éminents sculpteurs et constructeurs florentins. Aujourd'hui, dans la carrière revitalisée de Montraker, une école internationale de sculpture a lieu pendant les mois d'été. De jeunes sculpteurs de Croatie et du monde perpétuent la tradition de Vrsar depuis 1991, la traduisant en un art original et frais. Dans l'espace ouvert, l'école de taille de pierre du sculpteur, sous la direction d'un mentor de sculpteur, offre aux touristes un aperçu unique du traitement de la pierre, qui passe d'une forme de bloc à des formes artistiques inhabituelles. Les sculptures des ateliers précédents peuvent souvent être vues sur les plages, les parkings, le front de mer de Vrsar et dans les parcs de la ville.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Počeci klesarstva, kakvo danas poznajemo u Istri datiraju iz 1723. godine, kada se prvi put odvajaju klesari od rudara. Javlja se umijeće dekoriranja klesarskom tehnikom crkvenih ciborija, oltarnih pregrada i propovjedaonica na kojima pretežu motivi pletera. Prva kamenoklesarska zadruga Istre osnovana je 1905. godine u Tarskoj vali pokraj Novigrada. U sklopu nje su djelovali klesari koji su svoj zanat izučavali u kolijevki klesarstva, Italiji. Počinju se javljati različiti pravci u svim dijelovima Istre i takav trend se zadržao do danas. Vrsar s ponosom nosi ime “kiparski grad”. Posebno je važna kiparska škola Montraker, koja je dobila ime po istoimenom starom kamenolomu. Prema nekim podacima, rovinjsko-vrsarskim kamenom služili su se i istaknuti firentinski kipari i graditelji. U današnje vrijeme, u revitaliziranom kamenolomu Montraker, tijekom ljetnih mjeseci održava se internacionalna kiparska škola. Mladi kipari iz Hrvatske i svijeta od 1991. godine nastavljaju vrsarsku tradiciju, pretačući je u originalnu i svježu umjetnost. Na otvorenom prostoru kiparska klesarska škola, pod vodstvom mentora kipara, pruža turistima jedinstveni uvid u obradu kamena koji iz forme bloka postaju neobične umjetničke forme. Skulpture iz prijašnjih radionica često se mogu vidjeti na plažama, auto-kampovima, vrsarskoj rivi i u gradskim parkovima.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Stonework - Brač

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Jusqu'à treize variétés différentes de pierre sont exploitées à Brač, et beaucoup d'entre elles sont connues non seulement en Croatie, mais aussi dans le monde entier, en raison de la longue tradition d'exploitation et d'utilisation de la pierre, ainsi que de sa qualité. Selon la carte géologique de la République de Croatie M 1:300000, tous les gisements dans lesquels des variétés sont exploitées sur l'île de Brač appartiennent aux calcaires rudistes du Crétacé supérieur (Cénomanien - Maastrichtien).
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Na Braču se eksploatira čak trinaest različitih varijeteta kamena, a mnogi od njih su poznati ne samo u Hrvatskoj, nego i diljem svijeta, zbog dugotrajne tradicije eksploatacije i upotrebe kamena, ali i njihove kvalitete. Prema Geološkoj karti Republike Hrvatske M 1:300000 sve naslage u kojima se eksploatiraju varijeteti na otoku Braču pripadaju gornjoj kredi – rudistni vapnenci (cenoman – mastriht).
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La variété la plus célèbre de toutes est certainement le calcaire blanc de Brac, appelé commercialement Veselje avec les versions unito et fiorito, et qui est « récolté » dans les carrières près de Pučišće. Il apparaît sur le marché sous le groupe « marbre » et est souvent appelé à tort « marbre blanc de Brač », bien qu'il ne s'agisse pas d'une roche métamorphique, mais sédimentaire. Veselje est un calcaire organogène du Crétacé supérieur qui contient des fragments de rudistes. Veselje et Kupinovo sont les noms des baies où la pierre est "récoltée", tandis que unito et fiorito représentent les variétés de construction. La version unito implique que dans la structure de la pierre, des fragments squelettiques de tailles relativement uniformes jusqu'à 2 mm sont répartis de manière homogène et, pétrographiquement, cela correspond au calcaire bioélastique du type pexton. C'est précisément pour cette raison que cette variété est particulièrement adaptée au travail avec des outils de maçonnerie et constitue le matériau le plus souvent utilisé par les étudiants de l'École de maçonnerie de Pučišća. La variante fiorito est une variété dans laquelle de gros fragments bruns (supérieurs à 2 mm) de squelettes de rudistes ou entiers sont dispersés à la base du type unito et correspond au type floutston. En raison de sa structure inhomogène et de ses fragments plus gros, il est moins adapté au traitement avec des outils en pierre que la version unito.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Najpoznatiji varijetet od svih svakako je bijeli brački vapnenac, koji se komercijalno naziva Veselje s inačicama unito i fiorito, a “bere” se u kamenolomima kraj Pučišća. Na tržištu se pojavljuje pod grupom “mramora”, te se često pogrešno naziva “bijeli brački mramor” iako se ne radi o metamorfnoj, već o sedimentnoj stijeni. Veselje je gornjokredni organogeni vapnenac koji sadrži fragmente rudista. Veselje i Kupinovo su imena uvala gdje se kamen “bere”, a unito i fiorito predstavljaju varijetete u građi. Inačica unito podrazumijeva da su u građi kamena skeletni fragmenti relativno ujednačenih veličina do 2 mm homogeno raspoređeni, a petrografski odgovara bio lastičnom vapnencu tipa pekston. Upravo zbog toga ovaj varijetet je iznimno pogodan za obradu klesarskim alatima, te je to materijal koji najčešće koriste učenici Klesarske škole u Pučišćima. Inačica fiorito je varijetet u kojem su u osnovi tipa unito razbacani krupni (veći od 2 mm) smeđi fragmenti rudista odnosno cijeli skeleti, te odgovara tipu floutston. Zbog svoje nehomogene građe i većih fragmenata manje je pogodan za obradu klesarskim alatima u odnosu na inačicu unito.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Brač - buildings

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Sur l'île de Brač, ainsi que sur d'autres îles croates, on retrouve divers monuments de la vie sur pierre de l'Antiquité à nos jours : murs ou « tas », forts ou « forts », puis une forme particulière d'habitations « bunje ». , qui sont encore utilisés comme maisons de campagne.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Na otoku Braču kao i drugim hrvatskim otocima nalazimo različite spomenike života na kamenu od pradavnih vremena do danas: zidovi ili “gomile”, utvrde ili “gradine”, zatim poseban oblik nastambi “bunje”, koje se još uvijek koriste kao poljske kućice.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La pierre blanche de Brac était déjà connue des bâtisseurs dans l'Antiquité et de nombreux auteurs en ont parlé. En témoignent les nombreux vestiges de mines à ciel ouvert romaines abandonnées, de monolithes et de pièces sculptées, qui se trouvent dans la zone des colonies actuelles de Splitska et Škrip. La construction du palais et de Salona de Dioclétien en pierre de Brac est certainement la plus significative de cette époque. Les dignitaires romains ont construit leurs « villae rusticae » sur l'île de Brač.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Bijeli brački kamen graditeljima je poznat već u antičko doba o čemu su pisali mnogi autori. O tome svjedoče brojni ostaci rimskih napuštenih površinskih kopova, monolita, te klesanih komada, koji se nalaze u području današnjih naselja Splitska, odnosno Škrip. Iz tog doba svakako je najznačajnija izgradnja Dioklecijanove palače i Salone korištenjem bračkog kamena. Na otoku Braču gradili su rimski velikodostojnici svoje “villae rusticae”.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Comme chez les Romains, les carrières étaient vouées à la protection des dieux, à l'époque de Dioclétien, les carrières de Brac étaient sous la protection du dieu Héraclès. C'est précisément pour cette raison que l'image d'Héraclès (Hercule) a été gravée dans la roche vivante sous la forme d'un relief dans la carrière de Rasohe, et qu'un autel votif a été trouvé dans la carrière de Plate, érigé par le superviseur de la carrière Valerius Valerianus. Héraclès était considéré comme un protecteur par les tailleurs de pierre, en raison des dangers auxquels ils étaient exposés quotidiennement lorsqu'ils abattaient des blocs dans les carrières, les transportaient en glissant sur des rampes abruptes jusqu'à la mer, montaient à bord des galères et naviguaient dangereusement sur la capricieuse Adriatique.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Kako su kod Rimljana kamenolomi bili posvećeni zaštiti bogova, brački kamenolomi za vrijeme Dioklecijana bili su pod zaštitom boga Herakla. Upravo stoga je u kamenolomu Rasohe lik Herakla (Herkula) uklesan u živu stijenu u obliku reljefa, a u kamenolomu Plate nađen je zavjetni žrtvenik kojega je podigao nadzornik kamenoloma Valerius Valerianus. Herakla su kamenari smatrali zaštitnikom, radi opasnosti kojima su svakodnevno bili izloženi pri obaranju blokova u kamenolomima, transportu klizanjem po strmim rampama do mora, ukrcaju na galije i opasnoj plovidbi ćudljivim Jadranom.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Les premières carrières de Brač étaient Plate, Rasohe et Stražišća, à proximité immédiate des colonies de Škrip et Splitska. Le village de Škrip (Skripea), appelé "civitas" et "oppidium", est probablement le premier plus grand village de l'île de Brač et représente un véritable monument du patrimoine de la pierre. Les carrières de Brac ne faisaient pas partie des fameuses carrières pénales romaines - "latomiae", mais plutôt des carrières ordinaires - "lapidicinae". Dans ces carrières, les vestiges de la technologie romaine d'extraction des blocs ont été conservés. Aujourd'hui encore, les coupes faites à la main de ce qu'on appelle "pašarini" dont les dimensions étaient de 0,5×4×15 m.Il était utilisé pour ouvrir et préparer la masse rocheuse pour l'extraction de gros blocs, appelés "feta". La pierre était brisée de telle manière que des cales en bois étaient pressées dans les rainures précédemment sculptées, qui gonflaient et roulaient la pierre lorsqu'elles étaient remplies d'eau. De plus, les blocs de pierre étaient amenés aux tailleurs de pierre sur des rouleaux et à l'aide de leviers, de poulies et de cordes, pour être traités et enfin chargés dans le port.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Prvi kamenolomi na Braču bili su Plate, Rasohe i Stražišća, u neposrednoj blizini naselja Škrip i Splitska. Naselje Škrip (Skripea), spominje se kao “civitas” i “oppidium”, vjerojatno je prvo veće naselje na otoku Braču, a predstavlja pravi spomenik kamenarske baštine. Brački kamenolomi nisu ulazili u red zloglasnih rimskih kažnjeničkih kamenoloma – “latomiae” već u red običnih kamenoloma – “lapidicinae”. U tim kamenolomima očuvani su ostaci rimske tehnologije vađenja blokova. I danas zadivljuju ručno izrađeni usjeci tzv. “pašarini” čije su dimenzije bile 0,5×4×15 m. Time se otvarala i pripremala stijenska masa za vađenje velikih blokova tzv. “feta”. Kamen se lomio tako da je su u prethodno isklesane žljebove utisnuti drveni klinovi, koji su zalijevani vodom bubrili i odvaljivali kamen. Dalje su blokove kamena na valjcima, te pomoću poluge, kolotura i konopa, donosili klesarima na obradu, te konačno na utovar u luku.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Du IXe au XIIe siècle, des églises préromanes ont été construites à Brač, dont 19 ont été conservées à ce jour, dont la plus célèbre est peut-être l'église Saint-Pierre. Nicolas (St. Mikule) près de Selc. Ensuite, il y a eu une certaine accalmie dans la construction jusqu'au début du XIVe siècle, où elle a recommencé à se développer plus fortement et, avec elle, la maçonnerie a également été renouvelée. À cette époque, les échos du début de la Renaissance sont également arrivés dans ces régions et la Dalmatie a produit trois sculpteurs et constructeurs célèbres : Juraj Dalmatinac, Andrija Alešija et Nikola Florentinac. Bien sûr, ils travaillaient la pierre de Brač, c'est pourquoi les carrières connaissent également un nouvel essor. De cette époque, le bâtiment le plus important est certainement la cathédrale de Sibenik, pour la construction de laquelle on a utilisé la pierre de Brac provenant de la carrière de Veselje. La pierre de Brač était également utilisée dans de nombreuses autres villes dalmates comme Trogir, Zadar ou Rab, ainsi que dans les villes italiennes d'Ancône, Mantoue, Tremiti et Rimini. De nouveaux « cafés » et « petrare » ouvrent à Selci, Pučišći, Sutivan, Nerežišći, Povlji et Dračevica.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Od 9. do 12. st. grade se na Braču predromaničke crkve, od kojih je do danas sačuvano čak njih 19 od kojih je možda najpoznatija crkvica sv. Nikole (Sv. Mikule) u blizini Selca. Nakon toga je sve do početka 14. stoljeća vladalo određeno zatišje u graditeljstvu, kada se ono ponovo počinje jače razvijati, a time se obnavlja i kamenarstvo. U to doba, odjeci rane renesanse pristižu i na ove prostore, a Dalmacija daje tri znamenita kipara i graditelja: Jurja Dalmatinca, Andriju Alešija i Nikolu Firentinca. Oni su, naravno, radili u bračkom kamenu, zbog čega i kamenolomi doživljavaju novi procvat. Iz tog doba, najznačajnija građevina je svakako šibenska katedrala za čiju je gradnju korišten brački kamen iz kamenoloma Veselje. Brački kamen korišten je i u brojnim drugim dalmatinskim gradovima poput Trogira, Zadra ili Raba, kao i talijanskim gradovima Ancona, Mantova, Tremiti i Rimini. Otvaraju se nove “kave” i “petrare” u Selcima, Pučišćima, Sutivanu, Nerežišćima, Povljima i Dračevici.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Sur l'île de Brač se trouve également le très célèbre monastère glagolitique de Blaca, également construit en pierre locale de Brač. Partout sur l'île se trouvent des vestiges de monastères paléochrétiens de l'Antiquité tardive et d'églises romanes primitives. Il est impossible d'énumérer tous les monuments et bâtiments construits en pierre de Brac depuis l'Antiquité jusqu'aux maîtres modernes, parmi lesquels Ivan Rendića et Ivan Meštrović, qui ont également réalisé des sculptures en pierre de Brac. Les vierges d'Ivan Rendić en pierre de Brac se trouvent dans les cimetières et jardins de Trieste, Zagreb (Zrinjevac, Mirogoj) et Dubrovnik. Le sculpteur Branko Dešković a grandi parmi les tailleurs de pierre Pučići.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Na otoku Braču, nalazi se i iznimno poznati glagoljaški samostan Blaca, izgrađen također od lokalnog bračkog kamena. Posvuda po otoku nalaze se ostaci kasnoantičkih starokršćanskih samostana i ranoromaničkih crkvica. Nemoguće je nabrojati sve spomenike i građevine izgrađene u bračkom kamenu od pradavnih vremena pa sve do modernih majstora od kojih ipak treba spomenuti Ivan Rendića i Ivana Meštrovića koji su također radili skulpture od bračkog kamena. Djevice Ivana Rendića od bračkog kamena nalaze se po grobljima i perivojima Trsta, Zagreba (Zrinjevac, Mirogoj) i Dubrovnika. Među pučiškim klesarima odrastao je i kipar Branko Dešković.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    À partir de la seconde moitié du XIXe siècle, la pierre de Brač était exportée vers l'Italie, l'Allemagne, l'Angleterre, l'Égypte, l'Amérique, la Hongrie et l'Autriche. De magnifiques bâtiments ont été construits en pierre de Brac, dont nous n'en retiendrons que quelques-uns : les palais de Budapest et de Vienne, le Parlement et les Nouveaux Palais de Vienne, l'intérieur du Parlement de Budapest, le Palais du Vice-roi de Trieste, le hall du Bâtiment des Nations Unies à New York. L'architecture contemporaine aime également utiliser la pierre de Brac, c'est pourquoi elle est incorporée dans le bâtiment de la Banque nationale croate à Zagreb, le mausolée Račić à Cavtat, le palais Meštrović à Split, la Maison croate des beaux-artistes (Pavillon Meštrović) à Zagreb. et bien d'autres bâtiments.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Od druge polovica 19. st. kamen se s Brača izvozio u Italiju, Njemačku, Englesku, Egipat, Ameriku, Mađarsku i Austriju. Od bračkog kamena načinjena su velebna zdanja od kojih izdvajamo samo neka: palače u Budimpešti i Beču, Parlament i Novi dvori u Beču, unutrašnjost Parlamenta u Budimpešti, Namjesnička palača u Trstu, predvorje zgrade Ujedinjenih naroda u New Yorku. Suvremena arhitektura također rado koristi brački kamen, pa je on ugrađen i u zgradu Hrvatske narodne banke u Zagrebu, Račićev mauzolej u Cavtatu, palaču Meštrović u Splitu, Hrvatski dom likovnih umjetnika (Meštrovićev paviljon) u Zagrebu i mnoge druge objekte.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Les informations sur l'activité économique de l'île parlent également de l'époque moderne et de l'importance de la pierre sur l'île de Brač. En 2006, un employé sur cinq sur l'île était employé dans la taille de pierre et, dans certaines localités comme Nerežič, Pučišća et Selc, près de 60 % des employés travaillaient dans cette industrie. Les données de l'étude minière et géologique montrent que jusqu'à 14 % de la superficie de l'île de Brač représente des zones potentielles pour l'obtention de pierres architecturales et de construction (contre 6 % de la superficie pour l'agriculture). Ainsi, les données sur le potentiel d'utilisation de l'espace et d'emploi de la population locale indiquent que la fabrication de pierre est l'activité la plus importante pour l'île de Brač et certaines de ses municipalités, encore plus importante que l'agriculture ou le tourisme, tant dans le passé qu'auparavant. aujourd'hui.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    O suvremenom dobu i značaju kamenarstva na otoku Braču govore i podaci o gospodarskoj djelatnosti na otoku. Tijekom 2006. godine svaki je peti zaposlenik na otoku bio zaposlen u kamenarstvu, a u nekim je naseljima poput Nerežiča, Pučišća i Selca gotovo 60% zaposlenika radilo u toj djelatnosti. Podaci iz Rudarsko-geološke studije pokazuje da čak 14% površine otoka Brača predstavljaju potencijalne površine za dobivanje arhitektonsko-građevnog kamena (usporedno za poljoprivredu 6% površine). Dakle, podaci o potencijalu korištenja prostora i zaposlenosti lokalnog stanovništva govore o tome da je kamenarstvo za otok Brač i neke njegove općine najznačajnija djelatnost, značajnija čak i od poljoprivrede ili turizma, kako u prošlosti tako i danas.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Brač- school

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Sur l'île de Brač, la première coopérative dalmate de tailleurs de pierre a été fondée à Pučišća en 1902 et à Selci en 1905, dans le but de rassembler les artisans et les carrières et de développer le travail de la pierre et la maçonnerie. La première scierie à deux scies a été construite en 1903 dans la région de la baie de Luka à Pučišći, puis en 1928 à Selci. Avec le développement des coopératives, le besoin d’une formation organisée des tailleurs de pierre est apparu. Dès 1906, des cours de tailleur de pierre étaient organisés en coopération avec l'école artisanale de Split et des examens de tailleur de pierre avaient lieu à Pučišća. Jusqu'alors, le métier de tailleur de pierre se transmettait du maître tailleur de pierre à ses apprentis, appelés compagnons. Cette méthode d'enseignement ne pouvait pas satisfaire la demande apparue avec l'avènement de la mécanisation et le 2 janvier 1909, l'École de formation avancée pour apprentis fut ouverte à Pučišća. L'école artisanale de Selci a commencé à fonctionner en 1907 et, après une interruption pendant la Première Guerre mondiale, elle a fonctionné jusqu'en 1943. Après la Seconde Guerre mondiale, la production de pierre a repris et les carrières ont été regroupées en 1947 dans l'entreprise "Brač", connue plus tard sous le nom de "Jadrankamen". La même année, l’école de maçonnerie de Pučišća commença ses travaux.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Na otoku Braču osnovana je Prva dalmatinska klesarska zadruga u Pučišćima 1902. godine, a u Selcima 1905. s namjerom okupljanja zanatlija i kamenoloma, te razvitka kamenarstva i klesarstva. Prva pilana s dva gatera izgrađena je 1903. u predjelu uvale Luke u Pučišćima, a zatim 1928. godine u Selcima. Razvitkom zadruga pojavila se potreba za organiziranim školovanjem klesara. Već 1906. godine u suradnji s Obrtničkom školom u Splitu organizirani su tečajevi klesarstva i polažu se ispiti za klesare u Pučišćima. Do tada se klesarski zanat prenosio s majstora klesara na njegove učenike zvane kalfe. Takav način poučavanja nije mogao zadovoljiti potražnju, koja je nastala pojavom mehanizacije, te se 2. siječnja 1909. godine otvara Škola usavršavanja za šegrte u Pučišćima. Obrtnička škola u Selcima započinje s radom 1907. godine, te s prekidom tijekom 1. svjetskog rata radi do 1943. godine. Nakon 2. svjetskog rata kamenarstvo se opet pokreće, pa se kamenolomi 1947. godine objedinjuju u poduzeće “Brač”, kasnije poznato pod nazivom “Jadrankamen”. Iste godine započinje s radom Klesarska škola u Pučišćima.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Les premiers locaux de l'école étaient situés sur un terrain appartenant à la Première Coopérative de Maçonnerie Dalmate (aujourd'hui scierie Storo), puis plus près du centre-ville et de l'église, sur un emplacement appelé "Granier". Le bâtiment scolaire actuel est reconnaissable à son entrée et à ses nombreux objets en pierre de valeur fabriqués par les étudiants sous la direction de leurs mentors. À l'école, depuis ses débuts jusqu'à aujourd'hui, on utilise le traitement manuel traditionnel de la pierre avec d'anciens outils romains, appelés outils "dentés". Aujourd'hui, l'école Klesarska est connue non seulement en Croatie mais aussi en Europe. Les étudiants de cette école avaient déjà des expositions indépendantes de leurs travaux (Zagreb, 1994, Paris, 1995). Ils ont participé à la foire de la pierre (Vérone, 1998 et Nuremberg, 1999). Jusqu'à 9 000 visiteurs visitent l'atelier de l'école de maçonnerie chaque année pendant la saison touristique. L'école de maçonnerie de Pučišća a célébré son 110e anniversaire en 2019. Sur le portail de l'école il y a un texte gravé : "Mon travail est honorable et beau. On m'appelle l'escarpe".
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Prvi prostori škole bili su smješteni na lokaciji u vlasništvu Prve dalmatinske klesarske zadruge (današnja Storo pilana), a nakon toga bliže centru mjesta i crkvi, na lokaciji pod nazivom “Granier”. Današnja školska zgrada prepoznatljiva je po svom ulazu, i brojnim vrijednim predmetima od kamena koje su izradili učenici uz vodstvo svojih mentora. U školi se od njezinih početaka, pa sve do danas njeguje tradicionalna ručna obrada kamena starim rimskim alatom, koriste se tzv. “zubati” alati. Danas je Klesarska škola poznata ne samo u Hrvatskoj nego i Europi. Učenici ove škole već su imali samostalne izložba učeničkih radova (Zagreb, 1994., Pariz, 1995.). Sudjelovali su na sajmu kamena (Verona, 1998. i Nurenberg, 1999.). Radionicu Klesarske škole godišnje posjeti do 9000 posjetitelja tijekom turističke sezone. Klesarska škola u Pučišćima obilježila je 110. godišnjicu postojanja 2019. godine. Na portalu škole stoji uklesan tekst: “Moje je posol častan i fin Mene zovu škarpelin”.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Grâce à son riche passé et à la préservation du patrimoine de pierre et de maçonnerie de l'île de Brač, ainsi qu'à l'amour des insulaires pour le « ramassage » et la sculpture de la pierre, il fonctionnera sûrement avec autant de succès dans l'avenir.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Zbog svoje bogate prošlosti i očuvanja kamenarske i klesarske baštine otoka Brača, te ljubavi otočana prema “branju” i klesanju kamena sigurno će jednako uspješno djelovati i u budućnosti
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Cities in stone

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    De nombreuses villes croates ont un noyau construit en pierre, comme Dubrovnik, Trogir, Hvar et Korčula. Les murs, palais, églises et maisons de ces villes créent une atmosphère unique et témoignent du savoir-faire des bâtisseurs croates.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Mnogi hrvatski gradovi imaju jezgre izgrađene od kamena, poput Dubrovnika, Trogira, Hvara i Korčule. Zidine, palače, crkve i kuće ovih gradova stvaraju jedinstvenu atmosferu i svjedoče o vještini hrvatskih graditelja.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Dubrovnik, également connue sous le nom de « Perle de l'Adriatique » ou de « Ville en pierre », est l'un des exemples d'architecture en pierre les plus impressionnants au monde. Située sur la côte de la mer Adriatique, Dubrovnik est une ancienne ville médiévale qui a gagné le statut de site du patrimoine mondial de l'UNESCO en raison de sa beauté et de l'état extraordinaire de son noyau historique préservé. Le symbole de la ville réside dans ses imposantes murailles, construites au fil des siècles. Cette immense forteresse de calcaire protégeait Dubrovnik des attaques et représente aujourd'hui le complexe de fortifications le mieux conservé d'Europe.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Dubrovnik, poznat i kao "Biser Jadrana" ili "Grad u Kamenu", jedan je od najupečatljivijih primjera kamene arhitekture u svijetu. Smješten na obali Jadranskog mora, Dubrovnik je stari srednjovjekovni grad koji je svojom ljepotom i izvanrednim stanjem sačuvane povijesne jezgre zaslužio status UNESCO-ve svjetske baštine. Simbol grada su njegove pozantne gradske zidine, izgrađene stoljećima. Ova masivna tvrđava od vapnenca pružala je zaštitu Dubrovniku od napada, a danas predstavlja najbolje očuvanu fortifikacijsku cjelinu u Europi.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Lorsque nous parlons de pierre à Trogir, nous ne pouvons ignorer son architecture unique, qui est principalement construite en pierre locale, y compris le marbre de Brac. Bâtiments en pierre, églises, palais et la cathédrale Saint-Pierre. Lovre est le cœur de Trogir et témoigne de la riche histoire et du patrimoine culturel de cette ville.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Kada govorimo o kamenu u Trogiru, ne možemo zaobići njegovu jedinstvenu arhitekturu koja je uglavnom izgrađena od lokalnog kamena, uključujući brački mramor. Kamene zgrade, crkve, palače i katedrala sv. Lovre čine srce Trogira i svjedoče o bogatoj povijesti i kulturnoj baštini ovog grada.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    La pierre joue également un rôle dans la formation du paysage de l'île de Hvar. Les terrasses en plaques de plâtre, traditionnellement construites en pierre pour cultiver des vignes et des oliveraies sur le terrain vallonné de l'île, sont souvent présentes dans les zones rurales. Ces constructions en cloisons sèches offrent non seulement une fonctionnalité dans l'agriculture, mais ajoutent également une valeur esthétique au paysage, créant une apparence visuelle unique.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Kamen također igra ulogu u formiranju pejzaža otoka Hvara. Suhozidne terase, koje su tradicionalno izgrađene od kamena kako bi se obrađivale vinogradi i maslinici na brdovitim terenima otoka, često su prisutne u ruralnim područjima. Ove suhozidne konstrukcije ne samo da pružaju funkcionalnost u poljoprivredi, već i dodaju estetsku vrijednost krajoliku, stvarajući jedinstvenu vizualnu pojavu.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    L'île de Korčula regorge de différents types de pierre, le matériau le plus couramment utilisé étant la pierre des côtes blanches de Dalmatie. Cette pierre se caractérise par une teinte plus claire et une haute qualité, ce qui en fait un choix populaire auprès des constructeurs. La plupart des bâtiments les plus impressionnants de l'île de Korčula, y compris la ville de Korčula, ont été façonnés avec cette pierre.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Otok Korčula obiluje različitim vrstama kamena, a najčešće korišteni materijal je kamen s bijelih obala Dalmacije. Ovaj kamen karakterizira svjetlija nijansa i visoka kvaliteta, što ga čini popularnim izborom za graditelje. Mnoge od najimpresivnijih građevina na otoku Korčuli, uključujući grad Korčulu, oblikovane su ovim kamenom.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## Croatian stone Masters

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Ivan Meštrović
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Ivan Meštrović
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Peut-être le sculpteur croate le plus célèbre de tous les temps, Ivan Meštrović a laissé une marque indélébile sur l'art mondial avec ses statues monumentales et ses œuvres sculptées. Ses œuvres combinent les styles traditionnel et moderne et nombre d'entre elles se trouvent partout en Croatie, ainsi que dans les musées du monde.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Možda najpoznatiji hrvatski kipar svih vremena, Ivan Meštrović je ostavio neizbrisiv trag u svjetskoj umjetnosti svojim monumentalnim kipovima i klesanim radovima. Njegova djela kombiniraju tradicionalne i moderne stilove, a mnoga od njih možete pronaći diljem Hrvatske, ali i u svjetskim muzejima.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Frano Krsinić
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Frano Krsinić
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Krsinić était un sculpteur extrêmement talentueux qui a également laissé une profonde marque sur la taille de pierre croate. Elle est connue pour ses reliefs, statues et monuments dans tout le pays. Ses œuvres combinent souvent des techniques traditionnelles avec des thèmes et styles modernes.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Krsinić je bio iznimno talentirani kipar koji je također ostavio dubok trag u hrvatskom klesarstvu. Poznat je po svojim reljefima, kipovima i spomenicima diljem zemlje. Njegova djela često kombiniraju tradicionalne tehnike s modernim temama i stilovima.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Juraj Dalmatinac
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Juraj Dalmatinac
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Vivant au XVe siècle, Juraj Dalmatinac était l'un des tailleurs de pierre les plus importants de Dalmatie. Son chef-d'œuvre est la cathédrale Saint-Pierre. Jakov à Šibenik, site classé au patrimoine mondial de l'UNESCO. Sa maîtrise de la pierre et de l'architecture a laissé une marque durable sur le paysage urbain de la Dalmatie.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Živeći u 15. stoljeću, Juraj Dalmatinac bio je jedan od najvažnijih klesara u Dalmaciji. Njegovo remek-djelo je katedrala sv. Jakova u Šibeniku, koja je UNESCO-ova svjetska baština. Njegovo majstorstvo u klesarstvu i arhitekturi ostavilo je trajan pečat na urbanom pejzažu Dalmacije.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Ivan Rendić
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Ivan Rendić
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Rendić était un célèbre sculpteur et tailleur de pierre croate qui a travaillé à la fin du 19e et au début du 20e siècle. Il est connu pour son travail sur de nombreuses églises dans toute la Croatie, ainsi que pour ses monuments publics et ses reliefs. Ses œuvres s'inspirent souvent de motifs religieux et de styles classiques.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Rendić je bio poznati hrvatski kipar i klesar koji je djelovao krajem 19. i početkom 20. stoljeća. Poznat je po svom radu na brojnim crkvama diljem Hrvatske, kao i po javnim spomenicima i reljefima. Njegova djela često su inspirirana religijskim motivima i klasičnim stilovima.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Slavomir Drinković
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Slavomir Drinković
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Tailleur de pierre plus contemporain, Drinković est connu pour ses œuvres sculptées abstraites et contemporaines. Ses œuvres combinent souvent des techniques traditionnelles de sculpture sur pierre avec une expression artistique contemporaine, créant des sculptures uniques et impressionnantes.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Suvremeniji klesar, Drinković je poznat po svojim apstraktnim i suvremenim klesanim radovima. Njegova djela često kombiniraju tradicionalne klesarske tehnike s suvremenim umjetničkim izrazom, stvarajući tako jedinstvene i impresivne skulpture.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

## In summary

<!-- <img class="interlaceleft" src="assets/img/interlace2.svg">   -->

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    Dans la Croatie d'aujourd'hui, la pierre naturelle est « récoltée » et utilisée dans diverses techniques et expressions de construction, architecturales, de maçonnerie et de sculpture, depuis l'Antiquité jusqu'à nos jours. La Dalmatie du Sud avec ses îles et la péninsule d'Istrie sont riches en pierres architecturales et de construction de haute qualité et en réserves qui, malgré des siècles d'exploitation, ne sont pas encore épuisées. En outre, dans les mêmes zones géographiques, il existe une tradition continue de formation de professionnels et d'artistes appropriés. Il ne fait aucun doute que la pierre a une perspective d'avenir et qu'elle représentera un segment économique très important dans les villes qui vivent de la pierre et de la pierre depuis des siècles et qui transmettront leur amour pour la pierre aux générations futures. travail, art et préservation de la tradition. Certaines carrières, bien que déjà fermées, connaissent des formes de conversion très spécifiques, c'est pourquoi elles sont utilisées à des fins touristiques et autres. Par exemple, Vinkuran, la plus ancienne carrière de la péninsule d'Istrie, est utilisée pour organiser divers événements tels que des expositions, des représentations théâtrales et d'autres événements similaires. Grâce à son acoustique extraordinaire, "Cavae Romane" est devenue une salle de concerts de haut niveau. Dans la carrière de Kirmenjak, dont la pierre est encore utilisée pour la reconstruction de Venise en raison de sa beauté et de ses propriétés exceptionnelles, se déroulent des ateliers internationaux de sculpture, dont le plus célèbre est sous la direction de l'école de sculpture Montraker. La carrière de Grožnjan-Kornerija produit de la pierre tendre et accueille des colloques de sculpture. Dans la carrière de Kanfanar, ils ont également des projets très sérieux pour l'utilisation future des salles souterraines, qui resteront après l'extraction de la pierre par la méthode souterraine.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Na prostorima današnje Hrvatske “bere” se i koristi prirodni kamen u raznim graditeljskim, arhitektonskim, klesarskim i kiparskim zahvatima i izričajima od antičkih vremena do danas. Južna Dalmacija s otocima i Istarski poluotok bogati su kvalitetnim arhitektonsko-građevnim kamenom i rezervama koje unatoč stoljetnoj eksploataciji još nisu iscrpljene. Pored toga, na istim geografskim prostorima postoji i kontinuirana tradicija obrazovanja odgovarajućih profesionalaca i umjetnika. Ne treba uopće dvojiti, da kamenarstvo ima svoju perspektivu i u budućnosti, te da će predstavljati vrlo važan gospodarski segment u gradovima koji žive za kamen i od kamena stoljećima, a ljubav prema tome prenose na sljedeće naraštaje kroz posao, umjetnost i očuvanje tradicije. Neki od kamenoloma, iako već zatvoreni, imaju vrlo konkretne oblike prenamjene, zbog čega se koriste u turističke i druge svrhe. Primjerice, najstariji kamenolom istarskog poluotoka, Vinkuran koristi se za održavanja raznih događanja poput izložbi, kazališnih predstava i drugih sličnih događanja. Zbog svoje izvanredne akustičnosti “Cavae Romane” su postale prostor za održavanje vrhunskih koncerata. U kamenolomu Kirmenjak, čiji se kamen i dalje zbog svoje iznimne ljepote i svojstava koristi za obnovu Venecije, održavaju se međunarodne kiparske radionice, a najpoznatija je pod vodstvom kiparske škole Montraker. U kamenolomu Grožnjan-Kornerija dobiva se podatan kamen i mjesto je održavanja kiparskih simpozija. U kamenolomu Kanfanar imaju također vrlo ozbiljne planove o budućoj namjeni podzemnih prostorija, koje će ostati nakon vađenja kamena podzemnom metodom.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>

<div class="parts">
<div class="fr bigContainer">
    <div class="fr stone">
    On peut donc conclure que la pierre et la maçonnerie, comme elles ont toujours représenté la source et la joie de vivre dans toutes les régions riches en gisements de pierre naturelle, représenteront la même chose à l'avenir soit à travers l'exploitation et la transformation. de la pierre ou par le développement d'autres activités, et surtout en nourrissant la tradition de l'expression artistique dans et autour de la pierre.
    </div>
    <div class="fr dust">
    </div>
</div>
<div class="cr bigContainer">
    <div class="cr stone">
    Može se, dakle, zaključiti da će kamen i kamenarstvo, kako su oduvijek predstavljali izvor i radost života u svim područjima bogatih ležišta prirodnog kamena, to isto predstavljati i u budućnosti bilo kroz eksploataciju i preradu kamena bilo razvojem drugih djelatnosti, a ponad svega njegujući tradiciju umjetničkog izražavanja u kamenu i o kamenu.
    </div>
    <div class="cr dust">
    </div>
</div>
</div>
